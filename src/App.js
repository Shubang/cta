import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import HomeComponent from './components/homeComponent/homeComponent';

class App extends React.Component {
  render() {

    return (
      <Router>
        <div>
          <Switch>
            <Route path="/" exact component={HomeComponent} />
          </Switch>
        </div>
      </Router>
    );
  }

}

export default App;
